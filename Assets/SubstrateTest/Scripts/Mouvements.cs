﻿using UnityEngine;
using System.Collections;
using Substrate;

public class Mouvements : MonoBehaviour {
	
	public enum gameMode{
		oculus,
		normal,
	};
	public gameMode gamemode = gameMode.normal;
	
	//Speed and gravity Variables
	public float speed = 4.0F;
    public float jumpSpeed = 8F;
	private float currYSpeed=0;
	public float gravity=0.4F;
    public float xSpeed = 180;
	public float ySpeed = 90;
	public float minYSpeed = -3F;
	public float clickTimeInterval = 0.1f;
	
	//Camera Informations
	public float angley;
	public float anglex;
	private Transform cam;
	private OVRCameraController camcontroller;
	private GameObject leftjoy;
	private GameObject rightjoy;
	
	public bool allowMovement = true;

	//Sound objects
	public GameObject soundPlayerObject;
	private SoundPlayer soundplayer; 
	
	//Controll delay controllers
	private bool isGravityOn=true;
	private float lastLeftClick=0;
	private float lastRightClick= 0 ;
	private float lastLampClick= 0 ;
	private float lastGravityClick= 0;
	private float lastMovementSound = 0;
	private float movementSoundInterval= 0.5f;
	
	private float hitdistance=5.0f;
	
		
	// Block to place
	private int currentBlock;
	
	//main world
	private subtest mapgenerateur;
	
	
	private CharacterController controller;	
	
	
	
	
	
		
	
	void Start () {
		camcontroller = (OVRCameraController) transform.FindChild("OVRCameraController").GetComponent<OVRCameraController>();
		
		//SixenseInput si = GameObject.Find("SixenseInput").gameObject.GetComponent<SixenseInput>();
		//isHydraConnected = SixenseInput.ble;
		
		leftjoy = (GameObject) gameObject.transform.FindChild("LeftJoy").gameObject;
		rightjoy =(GameObject) gameObject.transform.FindChild("RightJoy").gameObject;
		
		soundplayer = soundPlayerObject.GetComponent<SoundPlayer>();

		print ("layer: " + gameObject.layer);
		 currentBlock = BlockType.DIRT;
		
		controller = GetComponent<CharacterController>();
		//cam = transform.FindChild("Main Camera");
		if(gamemode==gameMode.oculus){ //oculus mode
			//enabling Oculus camera
			Transform go = transform.FindChild("OVRCameraController");
			go.FindChild("CameraLeft").camera.enabled=true;
			go.FindChild("CameraRight").camera.enabled=true;
			//Setting up oculus camera
			cam = go.FindChild("CameraLeft");
			camera.enabled=false;
		}
		else {
			Transform go = transform.FindChild("OVRCameraController");
			//disabling oculus Cameras
			go.FindChild("CameraLeft").camera.enabled=false;
			go.FindChild("CameraRight").camera.enabled=false;
			camera.enabled=true;
			//setting up camera
			cam = camera.transform; // normal mode
		}
			
		print (cam);
		//Setting up initial x and y angles 
		angley = transform.eulerAngles.y;
    	anglex = camera.transform.eulerAngles.x;
		GameObject mapgen = GameObject.Find("mapGeneratoeur");
		mapgenerateur = (subtest) mapgen.GetComponent(typeof(subtest));
		
		//setting up hitidistance: there's no point making it bigger than that since blocks won't be colliders pass this range
		hitdistance = mapgenerateur.getCollidersRange()/2;

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void FixedUpdate(){
		if(allowMovement){	//we can forbid movements for menues
			Mouvement();	
			pickControll();
		}
	}
	
	public void setPosition(UnityEngine.Vector3 pos){
		print ("setting player to: "+ pos);
		gameObject.transform.position	=pos;
	}
	
	public UnityEngine.Vector3 getPosition(){
		return gameObject.transform.position;	
	}
	
	/*
	 * this function checks for any input key and controls the behavior of each input
	 */
	void pickControll(){
		
		
		float fiyaa = Input.GetAxis("Fire1");
		Color32 clickLayer = new Color32(79,177,188,20);
		Color32 unclickLayer = new Color32(169,60,188,20);
		RaycastHit hit;
		Collider collider1;
		// getting hydra connecters
		SixenseHandController leftcontroller = (SixenseHandController) leftjoy.GetComponent<SixenseHandController>();
		SixenseHandController rightcontroller = (SixenseHandController) rightjoy.GetComponent<SixenseHandController>();
		
		
		UnityEngine.Vector3 vec=new UnityEngine.Vector3(0,0,0);// =  new UnityEngine.Vector3(Screen.width/4,Screen.height/2,0);
		
		
		Quaternion orientation = new Quaternion(0,0,0,0);
		UnityEngine.Vector3 orientationeuler;
		
		
		// Getting camera points of focus vectors to compute raycasts
		if(gamemode == gameMode.oculus){
			camcontroller.GetCameraPosition(ref vec);
			//vec += transform.position;
			//print (vec);
			camcontroller.GetCameraOrientation(ref orientation); //+ camcontroller.GetOrientationOffset()
			orientationeuler = orientation * UnityEngine.Vector3.forward;
		}
		else{
			orientationeuler=cam.camera.transform.forward;
			vec = cam.camera.transform.position;
		}
		
		//rayCasting
		Ray ray = new Ray(vec,orientationeuler); //.ScreenPointToRay(vec); //cam.camera.transform.position,cam.camera.transform.forward
		
		bool isHit = Physics.Raycast(ray,out hit, hitdistance);
		
		//We hilight the hit bloc
		if(isHit) mapgenerateur.updateHoveredBlock(hit.collider.gameObject);
		
		//Toogling gravity
		if( ( Input.GetKeyDown(KeyCode.G) || leftcontroller.getButton(SixenseButtons.ONE) ) && Time.time - lastGravityClick >= clickTimeInterval ){
			isGravityOn= !isGravityOn;
			print ("gravity: " + isGravityOn);
			lastGravityClick = Time.time;
		}
		
		//Putting lamps
		if((Input.GetKeyDown(KeyCode.L) || leftcontroller.getButton(SixenseButtons.THREE) )&& Time.time - lastLampClick >= clickTimeInterval){
			if (isHit) //remove layerMask if you remove it in line above
	        {
	            collider1 = hit.collider;
				print ("lighting");
				string side = getSide(hit);
				mapgenerateur.litBlock(collider1.gameObject,side);
				lastLampClick = Time.time;
			}
			else {
				print("oups");
			}
			
		}
		
		//Removing a block
		if ((fiyaa!=0 || leftcontroller.getButton(SixenseButtons.BUMPER)) && Time.time-lastLeftClick >= clickTimeInterval){
			if (isHit) //remove layerMask if you remove it in line above
	        {
	            //stores the object hit
	            collider1 = hit.collider;
				
				mapgenerateur.pickBlock(collider1.gameObject);
	            
				
				lastLeftClick = Time.time;
	        }
		}
		
		//puting a block
		else if((Input.GetAxis("Fire2")!=0 || rightcontroller.getButton(SixenseButtons.BUMPER)) && Time.time - lastRightClick >= clickTimeInterval){
			//print ("layzaaaa");
			if (isHit) //remove layerMask if you remove it in line above
	        {
	            //stores the object hit
	            collider1 = hit.collider;
				
				// we try to figure out which side of the block we clicked on
				string side = getSide(hit);
				mapgenerateur.putBlock(collider1.gameObject,side,currentBlock);
				lastRightClick = Time.time;
	
	        }
		}
		float sc;
		//Scrolling among aviables blocs to put
		if ((sc= Input.GetAxis("Mouse ScrollWheel")) != 0 || rightcontroller.getButton(SixenseButtons.TRIGGER) || leftcontroller.getButton(SixenseButtons.TRIGGER) ){
			if( sc!=0)
				currentBlock+=(int)(sc*10);
			else if(rightcontroller.getButton(SixenseButtons.TRIGGER))
				currentBlock ++;
			else if(leftcontroller.getButton(SixenseButtons.TRIGGER))
				currentBlock --;
			
			if(currentBlock>24){
				currentBlock = BlockType.STONE;	
			}
			else if(currentBlock<=0){
				currentBlock = 24;	
			}
			mapgenerateur.setPickedBlockTexture(currentBlock);
		}
	}
	
	/*
	 *
	 * Returns which side of a block we "hit" 
	 * 
	 */ 
	string getSide(RaycastHit hit){
		string side = "none";
		
		// We compare a normal of the hitray with the forward vector
		float angle = UnityEngine.Vector3.Angle(hit.normal,UnityEngine.Vector3.forward);
		
		
		if(Mathf.Approximately(angle, 0)){
			side = "back";
		}// back
		if(Mathf.Approximately(angle, 180)) side="front";
		if(Mathf.Approximately(angle, 90)){
			UnityEngine.Vector3 cross = UnityEngine.Vector3.Cross(UnityEngine.Vector3.forward,hit.normal);
			if(cross.x==-1) side="top";
			else if (cross.x == 1) side="bottom";;
			if(cross.y ==1 )  side="right";
			else if(cross.y == -1) side="left";
		}
		
		return side;
	}
	
	
	/*
	 * 
	 * This function handles the player movments and synchornizes the movements of the camera with the movements of the player
	 * 
	 * 
	 */
	void Mouvement(){
		
		//Getting joystick controlers
		SixenseHandController poscontroller = (SixenseHandController) leftjoy.GetComponent<SixenseHandController>();
		SixenseHandController camcontroller = (SixenseHandController) rightjoy.GetComponent<SixenseHandController>();
		UnityEngine.Vector3 moveDirection;
		
		//I believe I inverted the x and the y angle.
		
		if(gamemode == gameMode.oculus){
			moveDirection = new UnityEngine.Vector3(-poscontroller.getJoyStickX(), 0 , poscontroller.getJoyStickY()); // .getInput.GetAxis("Horizontal") Input.GetAxis("Vertical")
		}
		else{
			moveDirection = new UnityEngine.Vector3( Input.GetAxis("Horizontal"), 0 , Input.GetAxis("Vertical")); //  
		}
		if(gamemode == gameMode.oculus){
			anglex+= camcontroller.getJoyStickX()*45*0.06f; //Input.GetAxis("Mouse X")* 90 * (float)0.08;
			angley+= -camcontroller.getJoyStickY()*90*0.06f; //Input.GetAxis("Mouse Y")* 180 * (float)0.08;
		}
		else{
			anglex+= Input.GetAxis("Mouse X")* 90 * (float)0.08;
			angley+= Input.GetAxis("Mouse Y")* 180 * (float)0.08;
		}
		
		//We don't wanna let the player go higher than this 90 or lower than 85
		angley = Mathf.Clamp(angley,-90,85);
		// We dont rotate the camera the same way as the player. Indeed, the player doesn't rotate over the y abscysse
		Quaternion camRotate  = Quaternion.Euler(angley,anglex,0);
		Quaternion playerRotate  = Quaternion.Euler(0,anglex,0);
		transform.rotation = playerRotate;
		cam.transform.rotation = camRotate;
		
		//if gravity is on we add jump handeling and apply gravity to the player Otherwise we let him fly and modify the behavior of space and ctrl keys
		if(isGravityOn){
			if(!controller.isGrounded && currYSpeed>minYSpeed){
				currYSpeed-=gravity;
			}
			if(controller.isGrounded && currYSpeed!=0){
				//print ("reset currYSpeed");
				currYSpeed=0;
			}
			if ((Input.GetKey(KeyCode.Space) || camcontroller.getButton(SixenseButtons.FOUR) )  && controller.isGrounded ){//
				
				currYSpeed=jumpSpeed;
			}
		}
		else{
			if (Input.GetKey(KeyCode.LeftControl) || camcontroller.getButton(SixenseButtons.TWO)){
				moveDirection.y=-1;
			}
			if (Input.GetKey(KeyCode.Space)|| camcontroller.getButton(SixenseButtons.FOUR) ){//
				moveDirection.y=1;
			}
		}
	
		
		
		// Applying rotation to movement vector 
		float anglexrad=(anglex*Mathf.PI/180)%(Mathf.PI*2); 
		float dirx = moveDirection.x;
		float dirz = moveDirection.z;
		moveDirection.x = 	-(dirx * Mathf.Cos(anglexrad) - dirz*Mathf.Sin (anglexrad));	// MT51 quand tu nous tiens (le moins il devrait pas etre la ! =° ) 
		moveDirection.z = 	dirx * Mathf.Sin(anglexrad) + dirz*Mathf.Cos (anglexrad);		// we are basically applying a rotation matrix.
		moveDirection.Normalize();
		moveDirection*=speed;	
		
		if(isGravityOn)
			moveDirection.y = currYSpeed;
		
		//print ("movedirection: " + moveDirection);	
		//if (moveDirection.x != 0 || moveDirection.z != 0 ){ // we moved
				//print ("anus");
//		controller.rigidbody.MoveRotation(playerRotate);
		controller.Move(moveDirection * Time.deltaTime);
		//}
		
		//playing stepsounds
		if((moveDirection.x !=0 || moveDirection.z !=0 ) && Time.time - lastMovementSound > movementSoundInterval ){
			soundplayer.playStep();	
			lastMovementSound = Time.time;
		}
		
	}
	
}
