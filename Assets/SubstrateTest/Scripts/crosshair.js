﻿var crosshairTexture : Texture2D;
var position : Rect;
static var OriginalOn = true;
 
function Start()
{
	
    position = Rect((Screen.width - crosshairTexture.width/30) / 2, (Screen.height - 
        crosshairTexture.height/30) /2, crosshairTexture.width/30, crosshairTexture.height/30);
       	
}
function OnGUI()
{
    if(OriginalOn == true)
    {
        GUI.DrawTexture(position, crosshairTexture);
    }
}