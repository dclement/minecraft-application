using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class AudioPlayer: MonoBehaviour{
	
	
	public List<AudioClip> musicList;
	
	private int currmusic =0;
	
	void Start(){
		if(musicList.Count!=0){
			this.audio.clip = musicList[currmusic];
			this.audio.Play();
		}
		
		
	}
	
	void Update(){
		
		if(!this.audio.isPlaying){
			currmusic ++;
			if(currmusic>=musicList.Count){
				currmusic =0;	
			}
			this.audio.clip = musicList[currmusic];
			this.audio.Play();
		}
		
	}
	
	

}