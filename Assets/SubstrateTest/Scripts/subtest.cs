﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Substrate;
using Substrate.Core;
using System.Threading;

public class subtest : MonoBehaviour {
	
	//layers to handle blocks not colliding with the player such as plants
	public const int noncollidinglayer = 9;
	public const int collidinglayer = 8;	
	
	//Block Models
	private GameObject cubeModel;
	private GameObject stairsModel;
	private GameObject plantModel;
	
	
	public bool setSpawnPoint = false;
	
	private GameObject Lamp;
	
	public Texture2D loadingBarTexture;
	
	//world to load
	public string worldName = "world";
	
	//usefull for menu base worlds
	public bool enableGui = true;
	
	//Sound player objects
	public GameObject soundPlayerObject = null;
	private SoundPlayer soundplayer; 
	public bool enableSound = true;
	
	
	private Queue toload;
	private Queue todelete;
	
	//Map array
	private List<GameObject>[][][] loadedblocs;
	
	//defines how much of the world to load
	public int loadperimeter = 20; // n blocks away, 
	public int miny=60,maxy=100;
	
	//GameObject in which to put loaded blocks 
	public GameObject worldParent=null;
	
	//How many cubes to load at each update is computd 
	private int cubePerUpdate;
	
	//Player object
	private GameObject player;
	private IBlockManager bm;
	
	//not used anymore
	private BlockCache blockcache;
	
	private UnityEngine.Vector3 prevpos; 
	//dictionary containing block textures. the int corresponds to the block ID, the list of texture is all the different texture this block can take example: wood
	private Dictionary<int, List<UnityEngine.Texture2D> > texturesdic;
	
	//Number of blocks currently loaded
	private int blockcount= 0;
	
	//Number of unused blocks to keep
	private int blockPoolLimit = 2000; 	
	private Rect guirect;
	
	private int currentBlock=BlockType.DIRT; // block we'll be placing if player clicks
	
	//Different debugging and display layers
	private Color32 greenlayer;	
	private Color32 collidersLayer;
	private Color32 defaultLayer;
	private Color32 cacaLayer;
	private Color32 hoverLayer; 
	
	//Block higlighting 
	private Color32 currHoveredBlockLayer;
	private GameObject currHoveredBlock = null;
	
	private Shader diffuse;
	private Shader transparentDiffuse;
	
	
	//differents sets to have characteristics about blocks
	private HashSet<int> encounteredblocks;
	private HashSet<int> nondisplayedblocks;
	private HashSet<int> layeredBlocks;
	private HashSet<int> noncollidingblocks;
	private HashSet<int> transparentBlocks;
	private HashSet<int> stairsBlocks;
	private HashSet<int> plantBlocks;
	private HashSet<int> multitexturedBlocks;
	
	private Material watermat;
	private List<BoxCollider>[][][] colliders;
	public int collidersrange=7;
	
	//pools of nonused blocks that can be reused. 
	private Queue<GameObject> cubePool;
	private Queue<GameObject> stairsPool;
	
	
	public int getLoadCount(){
		return toload.Count;	
	}
	
	/*
	 * returns the coordinates of block within the loadedblock referencial 
	 */
	UnityEngine.Vector3 getCoordinatesFromBlock(GameObject block){
		UnityEngine.Vector3 blockposition = block.transform.position;
		UnityEngine.Vector3 currpoz = getPlayerPosition();
		UnityEngine.Vector3 offset = currpoz-blockposition;
		UnityEngine.Vector3 coordinates =new UnityEngine.Vector3(loadperimeter-Mathf.Floor(offset.x) ,Mathf.Floor( blockposition.y) , loadperimeter-Mathf.Floor(offset.z));
		return coordinates;
	}
	
	/*
	 * Loads all the neighbohrs of a given position and knowing the current user position. The blocks will be turned into a collider
	 */
	void loadNeighbours(int _x, int _y , int _z, UnityEngine.Vector3 currpos){
		int ID=0;
		
		//check the  6 position around the block and see if there's a block there
		List<UnityEngine.Vector3> neib  = new List<UnityEngine.Vector3>();
		List<int> IDs = new List<int>();
		int x= _x+Mathf.FloorToInt(currpos.x)-loadperimeter;
		int y = _y;
		int z = _z+Mathf.FloorToInt(currpos.z)-loadperimeter;
				
		if (!nondisplayedblocks.Contains(ID = bm.GetID(x+1,y,z)) && loadedblocs[_x+1][_z][_y].Count==0){
				neib.Add(new UnityEngine.Vector3(_x+1,_y,_z));
				IDs.Add (ID);
		}
		if (!nondisplayedblocks.Contains(ID = bm.GetID(x-1,y,z)) && loadedblocs[_x-1][_z][_y].Count==0){
				neib.Add(new UnityEngine.Vector3(_x-1,_y,_z));
				IDs.Add (ID);
		}
		if (!nondisplayedblocks.Contains(ID = bm.GetID(x,y+1,z)) && loadedblocs[_x][_z][_y+1].Count==0){
				neib.Add(new UnityEngine.Vector3(_x,_y+1,_z));
				IDs.Add (ID);
		}
		if (!nondisplayedblocks.Contains(ID = bm.GetID(x,y-1,z))&& loadedblocs[_x][_z][_y-1].Count==0){
				neib.Add(new UnityEngine.Vector3(_x,_y-1,_z));
				IDs.Add (ID);
		}
		if (!nondisplayedblocks.Contains(ID = bm.GetID(x,y,z+1))&& loadedblocs[_x][_z+1][_y].Count==0){
				neib.Add(new UnityEngine.Vector3(_x,_y,_z+1));
				IDs.Add (ID);
		}
		if (!nondisplayedblocks.Contains(ID = bm.GetID(x,y,z-1))&& loadedblocs[_x][_z-1][_y].Count==0){
				neib.Add(new UnityEngine.Vector3(_x,_y,_z-1));
				IDs.Add (ID);
		}
		
		// we add all the blocks
		int i =0; 
		foreach(UnityEngine.Vector3 vect in neib){
			//we can't enqueu them cause we need them right away to add them to the new colliders
			drawBlock(vect,currpos,currpos,IDs[i],0);
			addCollider((int) vect.x, (int) vect.y, (int) vect.z,currpos);
			i++;
		}
			
	}
	
	private UnityEngine.Vector3 fixedPlayerPosition;
	
	/*
	 * Those two function act directly over the Player to set and get its position
	 * 
	 */
	UnityEngine.Vector3 getPlayerPosition(){
		if(player != null){
			Mouvements mv = (Mouvements) player.GetComponent(typeof(Mouvements));
			return mv.getPosition();
		}
		return fixedPlayerPosition;
	}
	
	void setPlayerPosition(UnityEngine.Vector3 pos){

		if(player != null){
			Mouvements mv = (Mouvements) player.GetComponent(typeof(Mouvements));
			mv.setPosition(pos);
		}
		else{
			fixedPlayerPosition = pos;
		}
		
	}
	
	//game Gui, pretty simple...
	void OnGUI(){ 
		if(enableGui){	
			UnityEngine.Vector3 currpoz= getPlayerPosition();
			GUI.DrawTexture(guirect,texturesdic[currentBlock][0],ScaleMode.ScaleToFit,true);
			GUI.DrawTexture(new Rect(80,10,toload.Count/10,15),loadingBarTexture,ScaleMode.StretchToFill,true);
			GUI.TextArea(new Rect(10, 80, 200, 85), "Block Count: " + blockcount + "\nBlock Pool Size: " + (cubePool.Count+stairsPool.Count) + "\nBlocks to load: " + toload.Count  + "\nCoords: x:" + currpoz.x + " y:" + currpoz.y + " z:" + currpoz.z, 200);
		}
	}	
	
	//set the current picked block texture
	public void setPickedBlockTexture(int blocktype){
		if( texturesdic.ContainsKey(blocktype) ){ // if the texture has been loaded, we map it to the cube.
				//obj.renderer.material.mainTexture = texturesdic[ID];
				currentBlock = blocktype;
							//loadedblocs[x][z][y].renderer.material.shader = Shader.Find("Specular");
//						obj.renderer.material.SetColor("_Color", greenlayer);	
				
			}
	}
	
	// sets block at coordinates x,y,z in the loaded block referencial to be a collider
	void addCollider(int x, int y , int z, UnityEngine.Vector3 currpos){ //x,y and z are in the Loadedblocks referential and will be converted to the colliders referencial
		int colliderperim = Mathf.FloorToInt(collidersrange/2);
		int _x= Mathf.FloorToInt(x)+Mathf.FloorToInt(currpos.x)-colliderperim; //( _x,_y, _z are in the Block Manager referencial
		int _y = Mathf.FloorToInt(y) + Mathf.FloorToInt(currpos.y) - colliderperim;
		int _z = Mathf.FloorToInt(z)+Mathf.FloorToInt(currpos.z)-colliderperim;
		
		int ID = bm.GetID(_x,_y,_z);
		
		//set up which layer to put the block in ( plants or plain block) 
		int blocklayer=collidinglayer;
		if(noncollidingblocks.Contains(ID))
			blocklayer=noncollidinglayer; // it's not supposed to collide so we don't add it!
		
		int collperimeter = Mathf.FloorToInt(collidersrange/2);
		int offset = loadperimeter - collperimeter;
		
		List<GameObject> gol = loadedblocs[x][z][y]; // We retrieve the blocks at coordinate x,y,z in the laoded block referencial
		int collx=x-offset;
		int colly=y - Mathf.FloorToInt(currpos.y) + collperimeter ;
		int collz=z-offset;
		
		//we check if the collider is among the blocks that should be colliding
		bool valid = ( collx>=0 && collx < collidersrange && colly>= 0 && colly < collidersrange && collz >=0 && collz <collidersrange);
		if(gol.Count>0 && valid){
			
			foreach(GameObject blocks in gol){
				 // this will eventually be a problem if we have more than one block in the same spot, we won't be sure we picked the good one
				blocks.layer=blocklayer;
					
				BoxCollider newcollider = (BoxCollider) blocks.AddComponent("BoxCollider");
				colliders[collx][collz][colly].Add(newcollider); 
	//			 blocks.renderer.material.SetColor("_Color", collidersLayer);	//helps to know if a block is a collider
				//print ("x= " + x + " z= " + z + " y= " + y + " yen a:"+colliders[x][z][y].Count);
			}
		}
	}
	
	//sets the block in x,y,z in the loadedblocks referencial to the desired ID in the blockManager
	public void setBlock(int x, int y , int z, UnityEngine.Vector3 currpos, int blockID, int data=0){
		
		x= x+Mathf.FloorToInt(currpos.x)-loadperimeter;
		//y = y;
		z = z+Mathf.FloorToInt(currpos.z)-loadperimeter;
		bm.SetData(x,y,z,data);
		bm.SetID(x,y,z,blockID);
		
	}
	
	//Puts a block on the side of the given block
	public void putBlock(GameObject _block, string side,int ID){
		
		UnityEngine.Vector3 currpoz = getPlayerPosition();
		UnityEngine.Vector3 blockpoz = getCoordinatesFromBlock(_block);
		int x = (int) blockpoz.x;
		int y = (int) blockpoz.y;
		int z = (int) blockpoz.z;
		print (_block.transform.position);
		print ("x: " + x + " y: "+ y + " z: " + z);
		switch(side){
		case "top"  :
			y+=1;
			break;
		case "right" :
			x+=1;
			break;
		case "left" :
			x-=1;
			break;
		case "bottom" :
			y-=1;
			break;
		case "front":
			z-=1;
			break;
		case "back":
			z+=1;
			break;
		}
		print ("x: " + x + " y: "+ y + " z: " + z);
		//we can't enqueu them cause we need them right away to add them to the new colliders
		UnityEngine.Vector3 vect = new UnityEngine.Vector3(x,y,z);
		int data=0;
		//We update the block manager
		setBlock(x,y,z,currpoz,ID,data);
		drawBlock(vect,currpoz,currpoz,ID,data);
		addCollider((int) vect.x, (int) vect.y, (int) vect.z,currpoz);
		
		
	}
	
	public int getCollidersRange(){
		return collidersrange;
	}
	
	/*
	 * 
	 * removes a block from the game and load all of it's neighbohrs
	 * 
	 */
	
	public void pickBlock(GameObject _block){
		checkMovement(); // update world blocks
		
		UnityEngine.Vector3 currpoz = getPlayerPosition();
		UnityEngine.Vector3 blockpoz = getCoordinatesFromBlock(_block);
		int x = (int) blockpoz.x;
		int y = (int) blockpoz.y;
		int z = (int) blockpoz.z;
		
		if(loadedblocs[x][z][y].Count >0 ){
			//print ("count "+ loadedblocs[x][z][y].Count);
			foreach(GameObject block in loadedblocs[x][z][y]){
							//todelete.Enqueue(loadedblocs[x][z][y]);	// note: pretty useless.
				//if(block.Equals(_block)){
					//print ("blockpos = " + block.transform.position);
					Destroy(block); 
					
					blockcount--;
				
			}
			loadedblocs[x][z][y].Clear();		
			loadNeighbours(x,y,z,currpoz);
			setBlock(x,y,z,currpoz,BlockType.AIR);
		}
		if(enableSound)
			soundplayer.playDig();
		
	}
	
	/*
	 * Put a light on the given side of the given block
	 * 
	 */ 
	public void litBlock(GameObject _block,string side){
		//getting block position informations
		UnityEngine.Vector3 currpoz = getPlayerPosition();
		UnityEngine.Vector3 blockpoz = getCoordinatesFromBlock(_block);
		int x = (int) blockpoz.x;
		int y = (int) blockpoz.y;
		int z = (int) blockpoz.z;
		if(loadedblocs[x][z][y].Count >0 ){
			GameObject obj;
			foreach(GameObject block in loadedblocs[x][z][y]){
				
				//Computing movement and rotation to make the lamp appear in the right place
				float rotx=-70;
				float roty=0;
				float posx=0;
				float posy=-0.30f;
				float posz=0;
				print (side);
				if(side=="front"){
					roty=180;
					posz-=0.5f;}
				else if (side=="top"){
					rotx=-90;
					posy+=0.7f;	
				}
				else if (side=="back"){
					roty=0;
					posz+=0.5f;
				}
				else if(side == "left"){
					roty=270;
					posx-=0.5f;
				}
				else if(side=="right"){
					roty=90;
					posx+=0.5f;
				}
				UnityEngine.Vector3 angle = new UnityEngine.Vector3(rotx,roty, 0);
				UnityEngine.Vector3 position = new UnityEngine.Vector3(posx, posy, posz);

				
				//Instantiating the lamp and making it child of the block
				GameObject lamp = (GameObject) GameObject.Instantiate(Lamp,_block.transform.position,Quaternion.Euler(angle));
				lamp.transform.parent = _block.transform;
				lamp.transform.localPosition = position;
				
			}
		}
		
	}
	
	// Changes and updates the currently highlighted block
	public void updateHoveredBlock(GameObject newblock){
		if( newblock != currHoveredBlock){
			if(currHoveredBlock!=null){
				
				currHoveredBlock.renderer.material.SetColor("_Color",currHoveredBlockLayer);	
				
			}
			if( newblock !=null){
				currHoveredBlockLayer = newblock.renderer.material.GetColor("_Color");
				currHoveredBlock = newblock;
				currHoveredBlock.renderer.material.SetColor("_Color",hoverLayer);
			}
			
			
		}
		
	}
	
	/*
	 * the two following functions help building the texture dictionnary
	 * If two texture have the same ID they'll be put into the same list
	 * and will be accessible with the Block's DATA field
	 */ 
	void addTexture(int blockType , string resource){
		
		if(!texturesdic.ContainsKey(blockType)){
			texturesdic.Add(blockType, new List<Texture2D>());
		}
		texturesdic[blockType].Add(Resources.Load (resource)as Texture2D);
			
	}
	
	Texture2D getTexture(int blockType, int data){
		
		if(multitexturedBlocks.Contains(blockType)){
			List<Texture2D> l = texturesdic[blockType];
			return l[data%l.Count];	
		}
		else{
			return texturesdic[blockType][0];
		}
		
	}
	
	/*
	 * Texture loading function
	 * lots of dictionary filling =°
	 * 
	 */ 
	private void loadTextures(){
			
		diffuse = Shader.Find("Diffuse");
		transparentDiffuse = Shader.Find("Transparent/Diffuse");
		
		
		
		guirect=new Rect(10,10,60,60);
		
		//loads block models
		cubeModel = Resources.Load("cubeModel") as GameObject;
		stairsModel = Resources.Load("stairsModel") as GameObject;
		plantModel = Resources.Load("plantModel") as GameObject;
		
		//useless
		watermat = Resources.Load("waterMaterial") as Material;
		
		//All the block we don't wanna display at all
		// either they are air or we don't handle them properly yet
		nondisplayedblocks = new HashSet<int>(); 
		nondisplayedblocks.Add(BlockType.AIR);
		nondisplayedblocks.Add(BlockType.MYCELIUM);
		nondisplayedblocks.Add(BlockType.STONE_BUTTON);
		nondisplayedblocks.Add(BlockType.LEVER);
		nondisplayedblocks.Add(BlockType.SIGN_POST);
		nondisplayedblocks.Add(BlockType.WOOD_PLATE);
		nondisplayedblocks.Add(BlockType.WOOD_BUTTON);
		//nondisplayedblocks.Add(175); // grandes fleurs
		
		//Those block will have a specific model instead of the normal block one
		plantBlocks = new HashSet<int>();
		plantBlocks.Add (BlockType.CROPS);
		plantBlocks.Add (BlockType.CARROTS);
		plantBlocks.Add (BlockType.RED_ROSE);
		plantBlocks.Add (BlockType.YELLOW_FLOWER);
		plantBlocks.Add (BlockType.TALL_GRASS);
		plantBlocks.Add(BlockType.BROWN_MUSHROOM);
		plantBlocks.Add(BlockType.RED_MUSHROOM);
		plantBlocks.Add(BlockType.DEAD_SHRUB);
		plantBlocks.Add(BlockType.HAY_BLOCK);
		plantBlocks.Add(BlockType.MELON_STEM);
		plantBlocks.Add(BlockType.POTATOES);
		plantBlocks.Add(BlockType.PUMPKIN_STEM);
		plantBlocks.Add(BlockType.SAPLING);
		plantBlocks.Add(BlockType.SUGAR_CANE);	
		
		//Those blocks have several textures for the same ID
		multitexturedBlocks = new HashSet<int>();
		multitexturedBlocks.Add(BlockType.WOOD);
		multitexturedBlocks.Add(162); // grands arbres ( acacia / big oak )		
		multitexturedBlocks.Add(BlockType.WOOL);
		multitexturedBlocks.Add(BlockType.WOOD_PLANK);
		multitexturedBlocks.Add(BlockType.LEAVES);
		multitexturedBlocks.Add(BlockType.SAPLING);
		
		//those blocks textures are gray so we have to add them a green layer
		layeredBlocks = new HashSet<int>();
		layeredBlocks.Add(BlockType.TALL_GRASS);
		layeredBlocks.Add(BlockType.GRASS);
		layeredBlocks.Add(BlockType.LEAVES);
		layeredBlocks.Add(BlockType.VINES);
		layeredBlocks.Add(161);
		
		// Plant blocks, those blocks will be set up as colliders but in a different layer to prevent them from colliding with the player
		noncollidingblocks = new HashSet<int>();
		noncollidingblocks.Add (BlockType.RED_ROSE);
		noncollidingblocks.Add (BlockType.YELLOW_FLOWER);
		noncollidingblocks.Add (BlockType.TALL_GRASS);
		noncollidingblocks.Add (BlockType.RED_MUSHROOM);
		noncollidingblocks.Add (BlockType.BROWN_MUSHROOM);
		noncollidingblocks.Add (BlockType.VINES);
		noncollidingblocks.Add (BlockType.CROPS);
		noncollidingblocks.Add (BlockType.DEAD_SHRUB);
		noncollidingblocks.Add (BlockType.SAPLING);
		
		//Stairs: to display those blocks we will use the stairs model
		stairsBlocks=new HashSet<int>();
		stairsBlocks.Add (BlockType.WOOD_STAIRS);
		stairsBlocks.Add (BlockType.STONE_BRICK_STAIRS);
		stairsBlocks.Add (BlockType.SANDSTONE_STAIRS);
		stairsBlocks.Add (BlockType.BRICK_STAIRS);
		stairsBlocks.Add (BlockType.NETHER_BRICK_STAIRS);
		stairsBlocks.Add (BlockType.BIRCH_WOOD_STAIRS);
		stairsBlocks.Add (BlockType.COBBLESTONE_STAIRS);
		stairsBlocks.Add (BlockType.JUNGLE_WOOD_STAIRS);
		stairsBlocks.Add (BlockType.QUARTZ_STAIRS);
		stairsBlocks.Add (BlockType.SPRUCE_WOOD_STAIRS);
		
		//Those block we'll be set transparent with a texture
		transparentBlocks = new HashSet<int>();
		transparentBlocks.Add(BlockType.TALL_GRASS);
		transparentBlocks.Add(BlockType.RED_ROSE);
		transparentBlocks.Add (BlockType.YELLOW_FLOWER);
		transparentBlocks.Add(BlockType.GLASS);
		transparentBlocks.Add(BlockType.GLASS_PANE);
		transparentBlocks.Add(BlockType.VINES);
		transparentBlocks.Add(BlockType.SAPLING);
		
		//Setting up color layers
		defaultLayer = cubeModel.renderer.material.GetColor("_Color");
		greenlayer = new Color32(102,175,33,255);	// we setup the green layer that will be used to color blocks such as grass or leaves
		collidersLayer = new Color32(255,99,99,255);
		cacaLayer = new Color32(182,139,0,255);
		hoverLayer = new Color32(100,100,100,50);
		
		 //= new Color32(255,255,255,100);	
		texturesdic = new Dictionary<int, List<Texture2D> >();	// we build up the texture dictionary
		encounteredblocks = new HashSet<int>();
		string texturePath = "Textures/minecraft/textures/blocks/";
		
		//Building Texture dictionnary
		// for multi texture blocks, the order in which textures are added is important
		
		//Multi textured blocks
		addTexture(Substrate.BlockType.LEAVES,"leaves_acacia_opaque");
		addTexture(Substrate.BlockType.LEAVES,"leaves_big_oak_opaque");
		addTexture(Substrate.BlockType.LEAVES,"leaves_birch_opaque");
		addTexture(Substrate.BlockType.LEAVES,"leaves_jungle_opaque");
		addTexture(Substrate.BlockType.LEAVES,"leaves_oak_opaque");
		addTexture(Substrate.BlockType.LEAVES,"leaves_spruce_opaque");
		
		addTexture(Substrate.BlockType.WOOD,"log_oak");
		addTexture(Substrate.BlockType.WOOD,"log_spruce");
		addTexture(Substrate.BlockType.WOOD,"log_birch");
		addTexture(Substrate.BlockType.WOOD,"log_jungle");
		
		addTexture(162,"log_acacia");
		addTexture(162,"log_big_oak"); 
		
		
		addTexture(Substrate.BlockType.WOOD_PLANK,"planks_oak");
		addTexture(Substrate.BlockType.WOOD_PLANK,"planks_spruce");
		addTexture(Substrate.BlockType.WOOD_PLANK,"planks_birch");
		addTexture(Substrate.BlockType.WOOD_PLANK,"planks_jungle");
		addTexture(Substrate.BlockType.WOOD_PLANK,"planks_acacia");
		addTexture(Substrate.BlockType.WOOD_PLANK,"planks_big_oak");
		
		addTexture(Substrate.BlockType.WOOL,"wool_colored_white");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_orange");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_magenta");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_light_blue");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_yellow");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_lime");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_pink");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_gray");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_silver");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_cyan");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_purple");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_blue");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_brown");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_green");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_red");
		addTexture(Substrate.BlockType.WOOL,"wool_colored_black");		
		
		addTexture(Substrate.BlockType.SAPLING,"sapling_oak");
		addTexture(Substrate.BlockType.SAPLING,"sapling_spruce");
		addTexture(Substrate.BlockType.SAPLING,"sapling_birch");
		addTexture(Substrate.BlockType.SAPLING,"sapling_jungle");
		addTexture(Substrate.BlockType.SAPLING,"sapling_acacia");
		addTexture(Substrate.BlockType.SAPLING,"sapling_roofed_oak");
		
		
		//Mono textured blocks
		addTexture(Substrate.BlockType.GRASS,"grass_top");
		addTexture(Substrate.BlockType.DIRT,"dirt");
		addTexture(Substrate.BlockType.STONE,"stone");
		addTexture(Substrate.BlockType.COBBLESTONE,"cobblestone");
		addTexture(Substrate.BlockType.COBBLESTONE_STAIRS,"cobblestone");
		addTexture(Substrate.BlockType.TALL_GRASS,"tallgrass");
		addTexture(Substrate.BlockType.SAND,"sand");
		addTexture(Substrate.BlockType.BOOKSHELF,"bookshelf");
		addTexture(Substrate.BlockType.REDSTONE_LAMP_ON,"redstone_lamp_on");
		addTexture(Substrate.BlockType.REDSTONE_LAMP_OFF,"redstone_lamp_off");
		addTexture(Substrate.BlockType.CLAY_BLOCK, "clay");
		addTexture(Substrate.BlockType.SANDSTONE, "sandstone_bottom");
		addTexture(Substrate.BlockType.SANDSTONE_STAIRS, "sandstone_bottom");
		addTexture(Substrate.BlockType.GRAVEL,"gravel");
		addTexture(Substrate.BlockType.STATIONARY_WATER, "underwater");
		addTexture(Substrate.BlockType.LAVA, "lava_flow");
		addTexture(Substrate.BlockType.STATIONARY_LAVA, "lava_still");
		addTexture(Substrate.BlockType.WOOD_PLANK, "planks_oak");
		addTexture(Substrate.BlockType.WOOD_STAIRS, "planks_oak");
		addTexture(Substrate.BlockType.JUNGLE_WOOD_STAIRS, "planks_jungle");
		addTexture(Substrate.BlockType.SPRUCE_WOOD_STAIRS, "planks_spruce");
		addTexture(Substrate.BlockType.BIRCH_WOOD_STAIRS, "planks_birch");
		addTexture(Substrate.BlockType.BRICK_BLOCK,"brick");		
		addTexture(Substrate.BlockType.CACTUS, "cactus_side");		
		addTexture(Substrate.BlockType.CRAFTING_TABLE, "crafting_table_front");		
		addTexture(Substrate.BlockType.DIAMOND_BLOCK, "diamond_block");				
		addTexture(Substrate.BlockType.DIAMOND_ORE, "diamond_ore");						
		addTexture(Substrate.BlockType.FARMLAND,"farmland_wet");								
		addTexture(Substrate.BlockType.GLASS,"glass");
		addTexture(Substrate.BlockType.GLASS_PANE,"glass");
		addTexture(Substrate.BlockType.WOOD_DOOR, "door_wood_lower");
		addTexture(Substrate.BlockType.FURNACE, "furnace_front");						
		addTexture(Substrate.BlockType.GOLD_ORE,"gold_ore");						
		addTexture(Substrate.BlockType.GOLD_BLOCK,"gold_block");									
		addTexture(Substrate.BlockType.IRON_ORE, "iron_ore");									
		addTexture(Substrate.BlockType.IRON_BLOCK,"iron_block");									
		addTexture(Substrate.BlockType.LAPIS_ORE, "lapis_ore");									
		addTexture(Substrate.BlockType.MELON,"melon_side");									
		addTexture(Substrate.BlockType.HUGE_RED_MUSHROOM,"mushroom_block_skin_red");									
		addTexture(Substrate.BlockType.HUGE_BROWN_MUSHROOM, "mushroom_block_skin_brown");									
		addTexture(Substrate.BlockType.OBSIDIAN,"obsidian");									
		addTexture(Substrate.BlockType.PUMPKIN, "pumpkin_side");
		addTexture(Substrate.BlockType.QUARTZ_BLOCK, "quartz_block_lines_top");
		addTexture(Substrate.BlockType.QUARTZ_STAIRS, "quartz_block_lines_top");
		addTexture(Substrate.BlockType.SNOW, "snow");
		addTexture(Substrate.BlockType.STONE_BRICK, "stonebrick");
		addTexture(Substrate.BlockType.STONE_BRICK_STAIRS, "stonebrick");
		addTexture(Substrate.BlockType.STONE_SLAB, "stone_slab_top");
		addTexture(Substrate.BlockType.VINES, "vine");
		addTexture(Substrate.BlockType.WOOL, "wool_colored_brown");
		addTexture(Substrate.BlockType.RED_ROSE, "flower_rose");	
		addTexture(Substrate.BlockType.YELLOW_FLOWER, "flower_dandelion");
		addTexture(Substrate.BlockType.DEAD_SHRUB, "deadbush"); 
		addTexture(Substrate.BlockType.FENCE,"planks_oak"); // emmm 
		
				
	}
	// we need this if we're not using a player and if we want to have an external camera
	public UnityEngine.Vector3 spawnpos;
	
	// Use this for initialization
	void Awake () {
		// Loading world 
        string dest = "Minecraft server\\"+ worldName;
        
		//Setting Up the Block Pool
		cubePool = new Queue<GameObject>();
		stairsPool= new Queue<GameObject>();
		
		//Loading lamp
		Lamp = Resources.Load("Lamp") as GameObject;
		
		// Open our world
        NbtWorld world = NbtWorld.Open(dest);
		if(world==null){
			print ("failed to load world");	
			return;
		}
		bm = world.GetBlockManager();
		blockcache = new BlockCache(bm);
		/*tbm = new List<IBlockManager>();
		for( int k = 0 ; k< threadNumber;k++){
			NbtWorld wd = NbtWorld.Open(dest);
			tbm.Add(wd.GetBlockManager());	
		}*/
			
		
		toload = new Queue();
		todelete = new Queue();
		
		loadTextures();
		//Moving the player to the spawn point
		Substrate.SpawnPoint sp =  world.Level.Spawn;
	
		//Finding player 
		player = GameObject.FindWithTag("Player");
		print (player);
		
		// this is basically usefull in case we wanna use the world as a background menu, it'll set the spawn point to 0,0 making the world load in the right place
		if(setSpawnPoint){
			spawnpos = new UnityEngine.Vector3(0,90,0);
		}
		else{
			spawnpos = new UnityEngine.Vector3(sp.X,sp.Y+15,sp.Z);
			Screen.showCursor=false;
		}	
		if(soundPlayerObject ==null){
			enableSound = false;
		}
		
		
		if(enableSound){
			soundplayer = soundPlayerObject.GetComponent<SoundPlayer>();
		}
		
		// we create the block on which we will put the user at start. indeed, the colliders array will be empty at start and the player would fall
		GameObject socle = GameObject.CreatePrimitive(PrimitiveType.Cube);
		socle.transform.position = spawnpos + new UnityEngine.Vector3(0,-2,0); // we place the block below the user
		socle.transform.localScale = new UnityEngine.Vector3(3,1,3);	// we make it large and fancy
		
		setPlayerPosition(spawnpos);
		//initialise prevpos used to check if the player moved between two iterations
		prevpos = spawnpos;
				
		//computing the loaded block array range
		int range = loadperimeter*2+1;
		int total = range*range * (maxy-miny);
		
		//Computing the block recycling limit and the number of cubes to update per minutes	
		
		cubePerUpdate = (int) (loadperimeter * 2.2);
		blockPoolLimit= (int) (loadperimeter * loadperimeter * 4 *0.30); // a third of one layer of blocks
		
		
		//Building the colliders array
		print("collidersrange " + collidersrange);
		colliders = new List<BoxCollider>[collidersrange][][];
		for(int x = 0; x<collidersrange;x++){
			colliders[x] = new List<BoxCollider>[collidersrange][];
			for(int z = 0 ; z<collidersrange; z++){
				colliders[x][z] = new List<BoxCollider>[collidersrange];
				for( int y =0 ; y < collidersrange; y ++){
					colliders[x][z][y] = new List<BoxCollider>();
				}
			}
		}
		
		// Building the loaded block array 
		loadedblocs = new List<GameObject>[range][][];
		for (int x = 0; x < range; x++) 
		{
		   loadedblocs[x] = new List<GameObject>[range][];
			for (int z=0;z<range;z++){
				loadedblocs[x][z] = new List<GameObject>[256];
				for(int y=0; y<256;y++){
					loadedblocs[x][z][y] = new List<GameObject>();
				}
			}
		}
		
		//enqueing initial map
		UnityEngine.Vector3 currpoz = getPlayerPosition();
		for( int x = 0 ; x < range ; x++){
			for( int y = miny ; y < maxy ; y++){
				for( int z =0; z < range; z++){ // z=40 should be temporary
					enqueue(new UnityEngine.Vector3(x,y,z),currpoz,bm);
				}
			}
		}
			
        // The chunk manager is more efficient than the block manager for
        // this purpose, since we'll inspect every block
		print ("spawn point: "+ sp);
        IChunkManager cm = world.GetChunkManager();
		//ChunkRef cr = cm.GetChunkRef(sp.X/16,sp.Z/16);
		//displayChunk(cr);
		
	}
	
	//loads a block from the block manager into the engine
	//pos is the position within the loadedblock referencial
	void drawBlock(UnityEngine.Vector3 pos, UnityEngine.Vector3 currpos, UnityEngine.Vector3 poswhenqueued, int ID, int data=0){
		
		//Computing position in the loaded block array
		int range = loadperimeter*2+1;
		currpos.x = Mathf.Floor(currpos.x);
		currpos.y = Mathf.Floor(currpos.y);
		currpos.z = Mathf.Floor(currpos.z);
		UnityEngine.Vector3 nposwhenqueued = poswhenqueued;
		nposwhenqueued.x = Mathf.Floor(nposwhenqueued.x);
		nposwhenqueued.y = Mathf.Floor(nposwhenqueued.y);
		nposwhenqueued.z = Mathf.Floor(nposwhenqueued.z);
		
		UnityEngine.Vector3 npos = pos - (currpos-(nposwhenqueued)); // if the queue is too long, the player will have moved before the block is loaded, we therefore ajust the position of the block			
		npos.y =pos.y;
		//npos= pos; 
		
		int x= Mathf.FloorToInt(npos.x);
		int y= Mathf.FloorToInt(npos.y);	
		int z= Mathf.FloorToInt(npos.z);
		
		
		if(y<miny || y>maxy)// we should never get here
			print ("dafuk");
		if(x<0 || x>=range|| z<0 || z >= range ){ //|| loadedblocs[x][z][y].Count >0
			//we don't place the block since it's already been placed before !
			//print ("DROPIT!");
		}
		else{
			
			if (loadedblocs[x][z][y].Count >0){	//Note that this should actually never happen, the reason why it happens is that blocks are off by one and stored at the wrong index. A quick fix is to find their right index and to put them there
				//print ("missplaced, npos "  + npos + " pos " + pos +" currpos " + currpos + " poswhenqueued " + nposwhenqueued + " count: " + loadedblocs[x][z][y].Count );//Destroy(loadedblocs[x][z][y]);	
			/*	foreach(GameObject _blocks in loadedblocs[x][z][y]){
					Destroy(_blocks);	
				}
				loadedblocs[x][z][y].Clear(); */
			}
			
			//poz is the position of the block in the 3D model
			UnityEngine.Vector3 poz = new UnityEngine.Vector3( Mathf.Floor(pos.x +nposwhenqueued.x-loadperimeter),(int)pos.y ,Mathf.Floor(pos.z +nposwhenqueued.z-loadperimeter));
			GameObject obj=null;
			
			// check what kind of block we're considering 
			bool isStairs = stairsBlocks.Contains(ID); 
			bool isPlant = plantBlocks.Contains(ID);
			
			// We find which block to use, since there's two pool it'll depend on which kind of block it is. 
			if( (!isStairs && cubePool.Count>0) || (isStairs && stairsPool.Count>0) ){
				//reusing old block
				if(isStairs){
					obj = stairsPool.Dequeue();
				}
				else{
					obj = cubePool.Dequeue();
				}
				obj.transform.position=poz;
				obj.renderer.material.color = defaultLayer;
				Destroy(obj.transform.GetComponent<BoxCollider>()); //FIXME we shoudldn't need to remove the boxCollider Here
			}
			else{
				//Generating new Block
				if(isStairs){
					obj = (GameObject)  GameObject.Instantiate(stairsModel,poz,Quaternion.identity);	
					obj.GetComponent<BlockScript>().blockkind = BlockKind.stair;
				}
				else{
					obj = (GameObject)  GameObject.Instantiate(cubeModel,poz,Quaternion.identity);	
					obj.GetComponent<BlockScript>().blockkind = BlockKind.cube;
				}
				blockcount++;// incrementing the block count
			}
			
			// we remove lamps or old plants or any child object
			foreach(Transform child in obj.transform){
				Destroy(child.gameObject);	
			}
			
			if(isStairs){ // Stairs need to be rotated according to their data value
				UnityEngine.Vector3 rotateStairs= new UnityEngine.Vector3(-90,0,0);
				if(data==0)
					rotateStairs.y=270;
				if(data==1)
					rotateStairs.y=90;
				if(data==2)
					rotateStairs.y=180;
				if(data==3)
					rotateStairs.y=0;
				obj.transform.rotation=Quaternion.Euler(rotateStairs);
			}
			if(worldParent!=null){
				obj.transform.parent = worldParent.transform; // Setting the parent	if asked
			}
			
			loadedblocs[x][z][y].Add(obj);
			obj.layer=collidinglayer;	//We put the block into the colliding layer by default. ( it is set to the non colliding layer when used for a plant ) 
			Material objmat = obj.renderer.material; //caching material
			if( texturesdic.ContainsKey(ID) ){ // if the texture has been loaded, we map it to the cube.			
				
				if(!isPlant){									// if it's not a plant we handle it the normal way
					
					if(transparentBlocks.Contains(ID)){
		    			objmat.shader = transparentDiffuse;	
					}
					else{	
						objmat.shader = diffuse;	
					}
					
					objmat.mainTexture = getTexture(ID,data);// texturesdic[ID];
					if(layeredBlocks.Contains(ID))
						objmat.SetColor("_Color", greenlayer);	
				}
				else{											// if it's a plant, we remove all texture and color of the main block, generate the subblock and put it a texture
					GameObject plant =(GameObject) GameObject.Instantiate(plantModel, new UnityEngine.Vector3(0,0,0),Quaternion.identity);	
					objmat.shader= transparentDiffuse;
					objmat.SetColor("_Color",new UnityEngine.Color(1,1,1,0));
					objmat.mainTexture = null;
					//	obj.renderer.material.SetColor("_Color",defaultLayer);
					plant.transform.parent = obj.transform;
					plant.transform.localPosition=new UnityEngine.Vector3(0,0,0);
					GameObject c1 = plant.transform.GetChild(0).gameObject;
					c1.renderer.material.mainTexture = getTexture(ID,data);
					GameObject c2 = plant.transform.GetChild(1).gameObject;
					c2.renderer.material.mainTexture = getTexture(ID,data);
					if(layeredBlocks.Contains(ID)){
							c2.renderer.material.SetColor("_Color", greenlayer);
							c1.renderer.material.SetColor("_Color", greenlayer);
					}
					
				}
				
				
			}
			else{ // if we don't have any texture for this block
				objmat.mainTexture = null;
			}
			
			
			
		}
	}
	
	/*
	 * 
	 * Draws <number> block from the queue into the 3D model
	 * 
	 */
	public void drawQueue(int number){
		int i =0;
		
		UnityEngine.Vector3 currpos = getPlayerPosition();
		
		
		while( toload.Count >0 && i<number){
			i++;
			/*
			 * pos contains the coordinates relatively to the loadedblocks array
			 * poswhenqueued contains the coordinates of the player when the blocks were queued
			 * currpos contains the current position of the player. 
			 * 
			 */
			UnityEngine.Vector3[] poss = (UnityEngine.Vector3[]) toload.Dequeue();
			UnityEngine.Vector3 pos = poss[0];
			UnityEngine.Vector3 poswhenqueued = poss[1];
			drawBlock(pos,currpos,poswhenqueued,(int)poss[2].x,(int)poss[2].y);
			
		}
			//printlb();
	}
	
	/*
	 * checks if b1 collides with any other block in the game. this function was for debugging purposes, is slow and not used any more
	 */
	void colides(GameObject b1){
		UnityEngine.Vector3 poz = b1.transform.position;
		int range = loadperimeter*2+1;
		for (int x = 0 ; x<range; x++){
			for(int z = 0 ; z <range ; z++){
				for (int y= miny ; y<maxy; y++){
					if(loadedblocs[x][z][y].Count>0){
							UnityEngine.Vector3 curr = loadedblocs[x][z][y][0].transform.position;
						if( (Mathf.Abs(poz.x - curr.x) < 0.1) && (Mathf.Abs(poz.y - curr.y) < 0.1) && (Mathf.Abs(poz.z - curr.z) < 0.1)){
							if(!b1==loadedblocs[x][z][y][0]){
								print ("colliding !!! " +  poz);
							}
						}
					}
				}
			}
		}
		//print ("abacbon	");
	}
	
	void Start () {
	
	}
	
	void FixedUpdate(){
		
	}
	
	
	// Update is called once per frame
	void Update () {
		checkMovement(); // check if the user moved
		
		int todraw = (toload.Count/20)*cubePerUpdate * 7 / 100 - 6*cubePerUpdate;
		todraw = Mathf.Clamp(todraw ,30,cubePerUpdate);
//		print (todraw);
		drawQueue(todraw); // draw a couple of blocks
	}
	
	void checkMovement(){
		UnityEngine.Vector3 currpos = getPlayerPosition();
			
		int range = loadperimeter*2 + 1;

		  
		// we check if the player changed block before this update
		int lateralmove =  Mathf.FloorToInt(currpos.x) -Mathf.FloorToInt(prevpos.x); 
		int forwardmove = Mathf.FloorToInt(currpos.z) -Mathf.FloorToInt( prevpos.z); 
		int upwardmove=  Mathf.FloorToInt(currpos.y) -Mathf.FloorToInt(prevpos.y); 
		//print ("chate");
		if(lateralmove!=0) { 	   													// if he did, we change the loaded part of the world and the colliders
			updateLoadedBlocks(lateralmove,0,currpos); 								// note that monving on more than one axe at the same time seems buggy so we treat them separatly
			updateColliders(lateralmove,0,0,currpos);
		}
		if(forwardmove!=0) {
			updateLoadedBlocks(0,forwardmove,currpos);
			updateColliders(0,forwardmove,0,currpos);
		}
		if(upwardmove!=0){
			updateColliders(0,0,upwardmove,currpos);	
		}
		
		
	 	prevpos = currpos;	// updating currpos
	}
	
	
	
	void printlb(){ // prints all the loaded blocks, used for debug
			int range = loadperimeter * 2 + 1;
			string line = "";
			for(int x = 0 ; x<range;x++){
				for( int z = 0 ; z	<range ; z++){
					bool found =false; 
					for(int y = 0 ; y<256 ; y++){
							if (loadedblocs[x][z][y].Count>0 && !found){			
								line += "x | ";
								found=true;
							}
					}
					if(found==	false)
						line+= "o | ";
				}
				line +=" - \n";
			}
			print (line);
	}
	
	//Update the colliders array by moving all colliders to the requiered direction.
	// It removes colliders from block falling out of the array and adds colliders to blocks comming in the array range
	void updateColliders(int lateralmove, int forwardmove, int upwardmove, UnityEngine.Vector3 currpos){ 
		int range = collidersrange;	
		int xo = 0, zo = 0, xd = range,zd =range, yo =0 , yd = range; // origins and ends of blocs to remove
		int rxo = 0, rzo = 0, rxd = range , rzd = range, ryo = 0, ryd =range; //origins and ends of blocks to populate
		if (lateralmove<0){				// setting up delete and load boxes
			xo = range+lateralmove;
			rxd = -lateralmove;}
		else if(lateralmove>0){
			xd = lateralmove;
			rxo = range-lateralmove;}
		if(forwardmove>0){
			zd = forwardmove;
			rzo = range-forwardmove;
		}
		else if(forwardmove<0){
			zo = range+forwardmove;
			rzd = -forwardmove;
		}
		if(upwardmove>0){
			yd = upwardmove;
			ryo = range-upwardmove;
		}
		else if(upwardmove<0){
			yo = range + upwardmove;
			ryd = - upwardmove;
		}
		
		int xoffset = Mathf.Abs(xo-xd);
		int zoffset = Mathf.Abs(zo-zd);
		int yoffset = Mathf.Abs(yo-yd);
		int collperimeter = Mathf.FloorToInt(collidersrange/2);
		int offset = loadperimeter - collperimeter;
		
	//	print ("removing stuff " + "xo: " + xo + " zo: " + zo + " xd: " + xd + " zd: " + zd + " yo: " + yo + " yd: " + yd);
		//removing colliderComponents
		for(int x = xo; x<xd;x++){
			for(int z = zo; z<zd;z++){
				for( int y =yo; y<yd; y++){
					List<BoxCollider> coll = colliders[x][z][y];
					if( coll.Count !=0){
						foreach(BoxCollider colli in coll){
							//print ("hop " + colli);
							Destroy(colli);
						}
						colliders[x][z][y].Clear();
					}
					
					List<GameObject> gol = loadedblocs[x+offset][z+offset][y+Mathf.FloorToInt(currpos.y)-collperimeter];
					if(gol.Count>0){ // Debug
						foreach(GameObject blocks in gol){
							//blocks.renderer.material.SetColor("_Color", greenlayer);	//helps to know if a block is a collider
						}
					}
					
				}
			}
		}
		
		//printlb();
		
		// on bouge le contenu du tableau vers le haut, le bas, la gauche, ou la droite en fonction du besoin
		if(lateralmove<0){ // on décale vers la droite ( le joueur à bougé vers la gauche )			
			for(  int x = xo-1 ; x>=0 ; x--){
				for (int z = 0 ; z<range ; z++){
					for(int y = 0 ; y < range; y++){
						colliders[x+xoffset][z][y] = colliders[x][z][y];
						//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;
					}
				}
			}
		}
		else if (lateralmove>0){ // we slide to the left ( the player moved to the right)
			for(int x = xd ; x < range; x++){
				for(int z = 0 ; z < range ; z++){
					for(int y = 0 ; y < range; y++){
						colliders[x-xoffset][z][y] = colliders[x][z][y];
						//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;
					}
				}
			}
		}
		if ( forwardmove<0){ // we slide down, the player moved up
			for(int x = 0 ; x<range;x++){
				for(int z = zo-1 ; z>=0; z--){
					for(int y = 0 ; y < range; y++){
						colliders[x][z+zoffset][y] = colliders[x][z][y];
						//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;
					}
				}
			}
		}
		else if ( forwardmove>0 ){ // we slide up, the player moved down
			for (int x = 0 ; x<range ; x++){
				for (int z = zd ; z<range ; z++){
					for(int y = 0 ; y < range; y++){
						colliders[x][z-zoffset][y] = colliders[x][z][y];
						//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;
					}
				}
			}
		}
		if ( upwardmove<0){
			for(int x = 0 ; x<range;x++){
				for(int z = 0 ; z<range; z++){
					for(int y = yo-1 ; y >=0 ; y--){
						colliders[x][z][y+yoffset] = colliders[x][z][y];
						//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;
					}
				}
			}
		}
		else if ( upwardmove>0 ){ // we slide up, the player moved down
			for (int x = 0 ; x<range ; x++){
				for (int z = 0 ; z<range ; z++){
					for(int y = yd ; y < range; y++){
						colliders[x][z][y-yoffset] = colliders[x][z][y];
						//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;
					}
				}
			}
		}
		
		int colliderperim = Mathf.FloorToInt(collidersrange/2);
		
		// Setting up new colliders
		int blocklayer=collidinglayer;
		for(int x = rxo; x<rxd;x++){
			for(int z = rzo; z<rzd;z++){
				for( int y =ryo; y<ryd; y++){
					List<GameObject> gol = loadedblocs[x+offset][z+offset][y+Mathf.FloorToInt(currpos.y)-collperimeter];
					colliders[x][z][y] = new List<BoxCollider>();
					
					int _x= Mathf.FloorToInt(x)+Mathf.FloorToInt(currpos.x)-colliderperim; //( _x,_y, _z are in the Block Manager referencial
					int _y = Mathf.FloorToInt(y) + Mathf.FloorToInt(currpos.y) - colliderperim;
					int _z = Mathf.FloorToInt(z)+Mathf.FloorToInt(currpos.z)-colliderperim;
					
					int ID = bm.GetID(_x,_y,_z);
					
					if(noncollidingblocks.Contains(ID))	// si c'est pas un collider bennn on l'fait pas collider =]
						blocklayer=noncollidinglayer;
					else
						blocklayer=collidinglayer;
					
					if(gol.Count>0){
						foreach(GameObject blocks in gol){
							blocks.layer= blocklayer;	
							 // this will eventually be a problem if we have more than one block in the same spot, we won't be sure we picked the good one
							BoxCollider newcollider = (BoxCollider) blocks.AddComponent("BoxCollider");
							if(blocks.layer==collidinglayer)
								//blocks.renderer.material.SetColor("_Color", collidersLayer);	//helps to know if a block is a collider
							colliders[x][z][y].Add(newcollider);
							//print ("x= " + x + " z= " + z + " y= " + y + " yen a:"+colliders[x][z][y].Count);
						}
					}
					
				}
			}
		}
	}
	
	//update loaded blocks array by shifting all the blocks in the array on the given axe. it also queues all the new block that should be loaded
	void updateLoadedBlocks(int lateralmove, int forwardmove, UnityEngine.Vector3 currpos){
		
		int range = loadperimeter*2+1;	
		int xo = 0, zo = 0, xd = range,zd =range; // origins and ends of blocs to remove
		int rxo = 0, rzo = 0, rxd = range , rzd = range; //origins and ends of blocks to populate
		if (lateralmove<0){				// setting delete and load boxes up
			xo = range+lateralmove;
			rxd = -lateralmove;}
		else if(lateralmove>0){
			xd = lateralmove;
			rxo = range-lateralmove;}
		if(forwardmove>0){
			zd = forwardmove;
			rzo = range-forwardmove;
		}
		else if(forwardmove<0){
			zo = range+forwardmove;
			rzd = -forwardmove;
		}
		//printlb();
		//print ("removing stuff" + "xo: " + xo + " zo: " + zo + " xd: " + xd + " zd: " + zd);
		
		for(int x = xo; x<xd;x++){
			for(int z = zo; z<zd;z++){
				for( int y =miny; y<maxy; y++){
					if( loadedblocs[x][z][y].Count >0){
					//	print ("crackbooom");
						foreach(GameObject block in loadedblocs[x][z][y]){
							//todelete.Enqueue(loadedblocs[x][z][y]);	// note: pretty useless.
							//Destroy(block);
							BlockKind bk = block.GetComponent<BlockScript>().blockkind;
							if ((bk == BlockKind.cube && cubePool.Count > (int) (blockPoolLimit * 0.90)) || ( bk == BlockKind.stair && stairsPool.Count > (int) (blockPoolLimit*0.10)) ){ //we don't want more than 10 percent of stairs.
								Destroy(block);	
								blockcount--;
							}
							else{
								if(block.GetComponent<BlockScript>().blockkind == BlockKind.cube){
									cubePool.Enqueue(block);
								}
								else{		
									stairsPool.Enqueue(block);
								}
							}
						}
						
						loadedblocs[x][z][y].Clear();
					//	GameObject.Destroy(loadedblocs[x][z][y]);
					}
				}
			}
		}
		
		//printlb();
		
		int xoffset = Mathf.Abs(xo-xd);
		int zoffset = Mathf.Abs(zo-zd);
		
		// les blocks dont on ne se sert plus ont été supprimés.
		// on bouge le contenu du tableau vers le haut, le bas, la gauche, ou la droite en fonction du besoin
		if(lateralmove<0){ // on décale vers la droite ( le joueur à bougé vers la gauche )			
			for(  int x = xo-1 ; x>=0 ; x--){
				for (int z = 0 ; z<range ; z++){
						loadedblocs[x+xoffset][z] = loadedblocs[x][z];
						//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;
				}
			}
		}
		else if (lateralmove>0){ // we slide to the left ( the player moved to the right)
			for(int x = xd ; x < range; x++){
				for(int z = 0 ; z < range ; z++){
					loadedblocs[x-xoffset][z] = loadedblocs[x][z];	
					//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;
				}
			}
		}
		if ( forwardmove<0){ // we slide down, the player moved up
			for(int x = 0 ; x<range;x++){
				for(int z = zo-1 ; z>=0; z--){
					loadedblocs[x][z+zoffset] = loadedblocs[x][z];
					//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;
				}
			}
		}	
		else if ( forwardmove>0 ){ // we slide up, the player moved down
			for (int x = 0 ; x<range ; x++){
				for (int z = zd ; z<range ; z++){
					loadedblocs[x][z-zoffset] = loadedblocs[x][z];	
					//for(int y =0; y<256 ; y++) loadedblocs[x][z][y] = null;	
				}
			}
			
		}
	
		// populating new blocks				
		List<UnityEngine.Vector3> toEnqueueList = new List<UnityEngine.Vector3>();
		
		for(int x = rxo; x<rxd;x++){
			for(int z = rzo; z<rzd;z++){
				loadedblocs[x][z] = new List<GameObject>[256]; // we replace the removed line by a new fresh one
				for( int y =miny; y<maxy; y++){
					loadedblocs[x][z][y] = new List<GameObject>();
					//toEnqueueList.Add(new UnityEngine.Vector3(x,y,z));
					enqueue(new UnityEngine.Vector3(x,y,z),currpos,bm);
					
					
					//uncomment this to prevent enqueing
					/*int ID=0;
					int _x= Mathf.FloorToInt(x)+Mathf.FloorToInt(currpos.x)-loadperimeter;
					int _y = Mathf.FloorToInt(y);
					int _z = Mathf.FloorToInt(z)+Mathf.FloorToInt(currpos.z)-loadperimeter;
					
					if (!nondisplayedblocks.Contains(ID = bm.GetID(_x,_y,_z)) && isVisible(new UnityEngine.Vector3(_x,_y,_z)))	{ // we don't display unvisible blocks to make things ( much ) faster 
							int data = bm.GetData(_x,_y,_z);
							drawBlock(new UnityEngine.Vector3(x,y,z),currpos,currpos,ID,data);
							//UnityEngine.Vector3[] coord = new UnityEngine.Vector3[3] {pos,currpos,new UnityEngine.Vector3(ID,data,0)};	 // the 3rd vector contains the block ID, that's an ugly way to do things...
							//toload.Enqueue(coord);// we queue all the block that should be loaded.
								//	GameObject.Destroy(loadedblocs[x][z][y]);
					}*/
					
					
				}
			}
		}
		
		
		// Multithreading Test
		/*
		int numberPerThread = (int) toEnqueueList.Count / threadNumber;
		int leftovers = toEnqueueList.Count%threadNumber;
		List<Thread> threads = new List<Thread>();
		
		
		// Enqueuing takes a lot of time so we run it in separate threads
		for(int i =0; i< threadNumber ; i++){
			
			List<UnityEngine.Vector3> tl;
			if( i ==0){
				tl = toEnqueueList.GetRange(i*numberPerThread,numberPerThread + leftovers);
			}
			else{
				tl = toEnqueueList.GetRange(i*numberPerThread,numberPerThread );
			}
			Thread currt = new Thread(() => enqueueThreaded(tl ,currpos,bm) );
			print ("tl.Count: " + tl.Count);
			currt.Start();
			threads.Add(currt);
				
		}
		// we wait for every thread to finish	
		print ("all started");	
		foreach (Thread thread in threads)
    		thread.Join();
    	*/

		
		//printlb();
	}
	
	/*
	 * not used
	 */ 
	void enqueueThreaded(List<UnityEngine.Vector3> coords, UnityEngine.Vector3 currpos, IBlockManager blockmanager){
		print ("starting");
		for(int i =0; i < coords.Count; i++){
			enqueue(coords[i] , currpos, blockmanager);	
		}
		print ("done");
	}
	
	
	/*
	 * Puts a given block into the queue of blocks that should be loaded. It checks if the block is visible 
	 */
	void enqueue(UnityEngine.Vector3 pos, UnityEngine.Vector3 currpos, IBlockManager mbm){
		int ID=0;
		int x= Mathf.FloorToInt(pos.x)+Mathf.FloorToInt(currpos.x)-loadperimeter;
		int y = Mathf.FloorToInt(pos.y);
		int z = Mathf.FloorToInt(pos.z)+Mathf.FloorToInt(currpos.z)-loadperimeter;
		//lock(mbm){
			ID = mbm.GetID(x,y,z);
		//}
		bool displayed=false;
		//lock(nondisplayedblocks){
			displayed = !nondisplayedblocks.Contains(ID);
		//}
		if (displayed && isVisible(new UnityEngine.Vector3(x,y,z),mbm))	{ // we don't display unvisible blocks to make things ( much ) faster 
				int data = mbm.GetData(x,y,z);
				UnityEngine.Vector3[] coord = new UnityEngine.Vector3[3] {pos,currpos,new UnityEngine.Vector3(ID,data,0)};	 // the 3rd vector contains the block ID, that's an ugly way to do things...
				lock(toload){ // locking the queue
					toload.Enqueue(coord);// we queue all the block that should be loaded.
					//	GameObject.Destroy(loadedblocs[x][z][y]);
				}
		}
	}
		
	

	/*
	 * Checks wether a block is visible or not. 
	 * 
	 * A block is visible if it is itself a visible block ( not air ) 
	 * if at least one of its neighbohrs is not visible 
	 */ 
	bool isVisible(UnityEngine.Vector3 coord, IBlockManager mbm){
		int x= Mathf.FloorToInt(coord.x);
		int y = Mathf.FloorToInt(coord.y);
		int z = Mathf.FloorToInt(coord.z);
		
		/*return (nondisplayedblocks.Contains(bm.GetID(x-1,y,z)) || 
			nondisplayedblocks.Contains(bm.GetID(x+1,y,z)) ||
			nondisplayedblocks.Contains(bm.GetID(x,y-1,z)) ||
			nondisplayedblocks.Contains(bm.GetID(x,y+1,z)) ||
			nondisplayedblocks.Contains(bm.GetID(x,y,z-1)) ||
			nondisplayedblocks.Contains(bm.GetID(x,y,z+1)) ) ;
		*/
		
		int[] ids = new int[6];
		//lock(mbm){	
			ids[0] =mbm.GetID (x-1,y,z);
			ids[1] =mbm.GetID (x+1,y,z);
			ids[2] =mbm.GetID (x,y-1,z);
			ids[3] =mbm.GetID (x,y+1,z);
			ids[4] =mbm.GetID (x,y,z-1);
			ids[5] =mbm.GetID (x,y,z+1);
		//}
		bool isvisible=false; 
		
		for(int i = 0 ; i< 6; i++){
		//	lock(nondisplayedblocks){
				isvisible = (isvisible || nondisplayedblocks.Contains(ids[i]));	
		//	}
		//	lock(noncollidingblocks){
				isvisible = (isvisible || noncollidingblocks.Contains(ids[i]));	
		//	}
		//	lock(transparentBlocks){
				isvisible = (isvisible || transparentBlocks.Contains(ids[i]));	
		//	}
			
			if(isvisible)
				break;
		}
				
		return isvisible ;
	
	}
	
}
	
