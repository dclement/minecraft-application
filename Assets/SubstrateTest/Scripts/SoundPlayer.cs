using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class SoundPlayer: MonoBehaviour{
	
	 
	private List<AudioClip> digSounds;
	private List<AudioClip> stepSounds;
	
	private int stepsoundcount = 0;
	
	private AudioSource audiosource; 
	
	void Start(){
		// Load Sounds
		digSounds = new List<AudioClip>();
		stepSounds = new List<AudioClip>();
		
		for(int i =1; i<=6; i++){
			string path =  "step/grass" + i;
			AudioClip ac = (Resources.Load(path) as AudioClip);
			print (path  + " "+ ac);
			stepSounds.Add(ac);	
		}
		for(int i = 1 ; i <= 4 ; i++){
			digSounds.Add(Resources.Load("dig/grass" + i) as AudioClip);	
		}
		
		audiosource = gameObject.AddComponent<AudioSource>();
		
	}
	
	void Update(){
		// do Nothing
	}
	
	public void playDig(){
		audiosource.clip = digSounds[Random.Range(0, digSounds.Count)];
		audiosource.Play();	
	}
	
	public void playStep(){
		audiosource.clip = stepSounds[stepsoundcount];
		stepsoundcount++;
		if(stepsoundcount>=stepSounds.Count){
			stepsoundcount = 0;	
		}
		audiosource.Play();
	}

}