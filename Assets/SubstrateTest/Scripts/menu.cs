﻿    using UnityEngine;
    using System.Collections;
     
    public class menu : MonoBehaviour
    {
	
	
	private OVRGUI  		GuiHelper 		 = new OVRGUI();
	private GameObject      GUIRenderObject  = null;
	private RenderTexture	GUIRenderTexture = null;
	
	// Crosshair system, rendered onto 3D plane
	public Texture  CrosshairImage 			= null;
	private OVRCrosshair Crosshair        	= new OVRCrosshair();
	
	
    public GUISkin guiSkin;
    public Texture2D background, LOGO;
    public bool DragWindow = false;
    public string levelToLoadWhenClickedPlay = "mainscene";
    public string[] AboutTextLines = new string[0];
    public OVRCameraController CameraController;
     
    private string clicked = "", MessageDisplayOnAbout = "About \n ";
    private Rect WindowRect = new Rect((Screen.width / 2) - 100, Screen.height / 2, 200, 200);
    private float volume = 1.0f;
     
    private void Start()
    {
	
	OVRCameraController[] CameraControllers;
	CameraControllers = gameObject.GetComponentsInChildren<OVRCameraController>();
	CameraController = CameraControllers[0];
	if(CameraController != null)
	{
		CameraController.InitCameraControllerVariables();
		GuiHelper.SetCameraController(ref CameraController);
	}
	
	GUIRenderObject = GameObject.Instantiate(Resources.Load("OVRGUIObjectMain")) as GameObject;
		
	if(GUIRenderObject != null)
		{
			if(GUIRenderTexture == null)
			{
			int w = Screen.width;
			int h = Screen.height;

			if(CameraController.PortraitMode == true)
			{
				int t = h;
				h = w;
				w = t;
			}
			
			// We don't need a depth buffer on this texture
			GUIRenderTexture = new RenderTexture(w, h, 0);	
			GuiHelper.SetPixelResolution(w, h);
			GuiHelper.SetDisplayResolution(OVRDevice.HResolution, OVRDevice.VResolution);
		}
	}	
		
	// Attach GUI texture to GUI object and GUI object to Camera
	if(GUIRenderTexture != null && GUIRenderObject != null)
	{
		GUIRenderObject.renderer.material.mainTexture = GUIRenderTexture;
		
		if(CameraController != null)
		{
			// Grab transform of GUI object
			Transform t = GUIRenderObject.transform;
			// Attach the GUI object to the camera
			CameraController.AttachGameObjectToCamera(ref GUIRenderObject);
			// Reset the transform values (we will be maintaining state of the GUI object
			// in local state)
			OVRUtils.SetLocalTransform(ref GUIRenderObject, ref t);
			// Deactivate object until we have completed the fade-in
			// Also, we may want to deactive the render object if there is nothing being rendered
			// into the UI
			// we will move the position of everything over to the left, so get
			// IPD / 2 and position camera towards negative X
			Vector3 lp = GUIRenderObject.transform.localPosition;
			float ipd = 0.0f;
			CameraController.GetIPD(ref ipd);
			lp.x -= ipd * 0.5f;
			GUIRenderObject.transform.localPosition = lp;
			
			GUIRenderObject.SetActive(false);
		}
	}
	
		
		
    for (int x = 0; x < AboutTextLines.Length;x++ )
    {
    MessageDisplayOnAbout += AboutTextLines[x] + " \n ";
    }
    MessageDisplayOnAbout += "Press Esc To Go Back";
    }
     
    private void OnGUI()
    {
		
		/*if (Event.current.type != EventType.Repaint)
			return;
		*/
	    if (background != null)
	    	GUI.DrawTexture(new Rect(0,0,Screen.width , Screen.height),background);
	    if (LOGO != null && clicked != "about")
	    	GUI.DrawTexture(new Rect((Screen.width / 2) - 100, 30, 200, 200), LOGO);
	     
	    GUI.skin = guiSkin;
	    if (clicked == ""){
	    	WindowRect = GUI.Window(0, WindowRect, menuFunc, "VI50");
	    }
	    else if (clicked == "options"){
	    	WindowRect = GUI.Window(1, WindowRect, optionsFunc, "Options");
	    }
	    else if (clicked == "about")
	    {
	    	GUI.Box(new Rect (0,0,Screen.width,Screen.height), MessageDisplayOnAbout);
	    }else if (clicked == "resolution")
	    {
		    GUILayout.BeginVertical();
		    for (int x = 0; x < Screen.resolutions.Length;x++ )
		    {
			    if (GUILayout.Button(Screen.resolutions[x].width + "X" + Screen.resolutions[x].height))
			    {
			    	Screen.SetResolution(Screen.resolutions[x].width,Screen.resolutions[x].height,true);
			    }
		    }
		    GUILayout.EndVertical();
		    GUILayout.BeginHorizontal();
		    if (GUILayout.Button("Back")){
		    	clicked = "options";
		    }
		    GUILayout.EndHorizontal();
	    }
    }
     
    private void optionsFunc(int id)    {
	    if (GUILayout.Button("Resolution"))
	    {
	    clicked = "resolution";
	    }
	    GUILayout.Box("Volume");
	    volume = GUILayout.HorizontalSlider(volume ,0.0f,1.0f);
	    	AudioListener.volume = volume;
	    if (GUILayout.Button("Back"))
	    {
	    clicked = "";
	    }
	    if (DragWindow)
	    GUI.DragWindow(new Rect (0,0,Screen.width,Screen.height));
    }
     
    private void menuFunc(int id)
    {
    //buttons
    if (GUILayout.Button("Play Game"))
    {
    Application.LoadLevel(levelToLoadWhenClickedPlay);
    }
    if (GUILayout.Button("Options"))
    {
    clicked = "options";
    }
    if (GUILayout.Button("About"))
    {
    clicked = "about";
    }
    if (GUILayout.Button("Quit Game"))
    {
    Application.Quit();
    }
    if (DragWindow)
    GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));
    }
     
    private void Update()
    {
    if (clicked == "about" && Input.GetKey (KeyCode.Escape))
    clicked = "";
    }
    }