using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Substrate;
using Substrate.Core;

class CachedBlock{
	private float lastupdate; 
	// ref to where he is in the xxx
	
	private AlphaBlockRef blockRef; 
	
	public CachedBlock(AlphaBlockRef br){
		blockRef = br;
		lastupdate = Time.time;
	}
	
	public AlphaBlockRef getBlockRef(){
		return blockRef;	
	}
}

public class BlockCache{
	
	private IBlockManager bm; 
	private int hitcount =  0 ;
	private int misscount = 0 ; 
	
	private Dictionary<UnityEngine.Vector3,CachedBlock> cachedblocks;
	
	public BlockCache(IBlockManager _bm){
		bm= _bm;
		cachedblocks= new Dictionary<UnityEngine.Vector3, CachedBlock>();
	}
	
	
	
	
	private AlphaBlockRef getBlock(int x, int y, int z){
		CachedBlock returnblock;	
		bool gotval;
		
		UnityEngine.Vector3 coord = new UnityEngine.Vector3(x,y,z);
		
		if(gotval = cachedblocks.TryGetValue(coord,out returnblock)){
			hitcount++;	
		}
		else{
			misscount ++;
			
			returnblock = new CachedBlock(bm.GetBlockRef(x, y,z));
			cachedblocks.Add(coord,returnblock);
		}
		
		return returnblock.getBlockRef();
	}
	
		
	public int getId(int x, int y, int z){
		AlphaBlockRef bref = getBlock(x,y,z);
		return bref.ID;
	}
	
	public void setId(int x, int y, int z, int id){
		AlphaBlockRef br =  getBlock(x,y,z);
		br.ID = id;
	}
	
	public int getData(int x , int y , int z ){
		return getBlock(x,y,z).Data;	
	}
	
	public void setData(int x, int y , int z , int data){
		
		AlphaBlockRef br =  getBlock(x,y,z);
		br.Data = data;
	}
	
	public int getMissCount(){
		return misscount ;	
	}
	public int getHitCount(){
		return hitcount;
	}
	
	
}